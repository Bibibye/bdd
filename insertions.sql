USE projetbdd;


insert into country(name) values
       ('USA'),
       ('Japan'),
       ('France'),
       ('Germany')
;

insert into producer(lname, country_idcountry) values
       ('id software', 1),
       ('Crytek', 4),
       ('Unity Technologies', 1),
       ('Epic Games', 1),
       ('Valve Software', 1),
       ('Ubisoft', 3)
;
insert into engine(e_name, producer_idproducer) values
       ('id Tech 4', 1),
       ('CryEngine', 2),
       ('Unity', 3),
       ('Unreal Engine', 4),
       ('Source', 5),
       ('UBIArt Framework', 6)
;

insert into company(name, country_idcountry) values
       ('Nintendo', 2),
       ('Microsoft', 1),
       ('Sony', 2),
       ('Apple', 1),
       ('Google', 1)
;

insert into platforms(platform, company_idcompany) values
       ('3DS', 1),
       ('WiiU', 1),
       ('Xbox360', 2),
       ('Xbox One', 2),
       ('Windows', 2),
       ('PS3', 3),
       ('PS4', 4),
       ('Mac OS', 4),
       ('Android', 5)
;

insert into platforms_has_engine(platforms_idplatforms, engine_idengine) values
       (3, 1),
       (5, 1),
       (7, 1),
       (8, 1),
       (3, 2),
       (5, 2),
       (6, 2),
       (5, 3),
       (8, 3),
       (9, 3),
       (5, 4),
       (7, 4),
       (8, 4),
       (9, 4),
       (4, 4),
       (5, 5),
       (8, 5),
       (3, 5),
       (6, 5),
       (1, 6),
       (2, 6),
       (3, 6),
       (4, 6),
       (5, 6),
       (6, 6),
       (7, 6)
;

insert into producer_has_platforms(producer_idproducer, platforms_idplatforms) values
       (3, 1),
       (5, 1),
       (3, 2),
       (5, 2),
       (6, 2),
       (5, 3),
       (5, 4),
       (4, 4),
       (5, 5),
       (3, 5),
       (6, 5),
       (1, 6),
       (2, 6),
       (3, 6),
       (4, 6),
       (5, 6),
       (6, 6)
;


insert into producer_use_engine(producer_idproducer, engine_idengine) values
       (3, 1),
       (5, 1),
       (3, 2),
       (5, 2),
       (6, 2),
       (5, 3),
       (5, 4),
       (3, 5),
       (6, 5),
       (1, 6),
       (2, 6),
       (3, 6),
       (4, 6),
       (5, 6),
       (6, 6)
;

insert into users(uname,AES_password,uemail,u_fname,u_sname) values 
	('willian',AES_ENCRYPT('test1',CONCAT('willian','paiva')),'willian-munny@gmail.com','willian','paiva'),
	('chaharani',AES_ENCRYPT('test2',CONCAT('chaharani','chaidou')),'chaidou.chacha@gmail.com','chaharani','chaidou'),
	('Pierre',AES_ENCRYPT('test3',CONCAT('Pierre','Jean')),'jeandelaCourt@gmail.com','Pierre','Jean');

insert into users_has_platforms(users_idusers,platforms_idplatforms) values
	(1,1),
	(1,2),
	(1,9),
	(2,6),
	(2,1),
	(2,7),
	(2,3),
	(3,1),
	(3,3),
	(3,7),
	(3,5),
	(3,9),
	(3,2)
;

insert into genres(genre) values 
	('action'),
	('war'),
	('hack and slash'),
	('adventury')
;

insert into Games(title, year, producer_idproducer) values
	('Far Cry', 2013, 2),
	('Assassins creed', 2013, 3),
	('Doom 3', 2004, 1)
;

insert into Games_has_platforms(Games_idgames, platforms_idplatforms) values
       (1, 5),
       (1, 6),
       (1, 3),
       (2, 2),
       (2, 3),
       (2, 4),
       (2, 6),
       (2, 7),
       (3, 5),
       (3, 8)
;

insert into reviews(review, star_rating, Games_idgames, users_idusers) values
	('a lot of fun ', 7, 3,1),
	('it funny',6,2,2),
	('really? it s very bad !',2,1,3),
	('Are you kidding me? it s very cool !',9,2,2),
	('ok !',3,1,1)
;
	


insert into Games_has_genres(Games_idgames,genres_idgenres) values
	(3,1),
	(3,2),
	(2,1),
	(1,3)
	
;

insert into trailers(filepath,games_idgames) values
	('/home/trailers/farcry.mp4',3),
	('/home/trailers/assassin.mp4',2);

insert into keywords(keyword) values
	('action'),
	('combat'),
	('aventure'),
	('horreur'),
	('Jeux de rôle'),
	('casual game'),
	('shoot'),
	('actionrpg'),
	('sandbox'),
	('survival horror'),
	('jeux de rythme'),
	('mmorpg'),
	('fantastique'),
	('console'),
	('PC'),
	('Vol de combat'),
	('jeu de simulateur'),
	('jeu de combat spatiale'),
	('jeu de carte'),
	('jeu infiltration')
;

insert into Games_has_keywords(games_idgames, keywords_idkeywords) values
	(1,1),
	(1,3),
	(1,6),
	(1,9),
	(2,2),
	(2,1),
	(2,3),
	(2,5),
	(2,7),
	(3,4),
	(3,7)
;



